`timescale 1ns / 1ps

module PER1 (p_send, p_ack, p_data, p_clk, p_rst);
  input [1:0] p_send; // send da CPU
  input p_clk; // clock
  input p_rst; // reset
  
  output reg p_ack; // ack de recebimento
  
  input [15:0] p_data;
  reg [15:0] p1_data; // dados recebidos da CPU
  
  reg p_stt; // estado atual
  reg p_nstt; // próximo estado
  
  // Next State
  always @ (*)
  begin
    case (p_stt)
      // a=0
      0:
        begin
          if (p_send[1:1] == 1) p_nstt = 1;
        end
      // a=1
      1:
        begin
          if (p_send[1:1] == 0) p_nstt = 0;
        end
    endcase
  end
  
  // State
  always @ (posedge p_clk)
  begin
    if (p_rst == 1)
      p_stt <= 0;
    else
      p_stt <= p_nstt;
  end
  
  // Keep data
  always @ (p_stt)
  begin
    if (p_stt == 1)
      p1_data = p_data;
  end

  // Outputs
  always @ (*)
  begin
    p_ack = p_stt;
  end
endmodule