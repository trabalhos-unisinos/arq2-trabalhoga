`timescale 1ns / 1ps

module testbench;
  
  wire [15:0] data1; // dados para per 1
  wire [15:0] data2; // dados para per 2
  wire [1:0] c_send;
  wire p1_ack;
  wire p2_ack;

  // CPU
  reg c_clk;
  reg c_rst;
  
  CPU cpu(
    .c_rst(c_rst),
    .c_clk(c_clk),
    .c_send(c_send),
    .c_ack1(p1_ack),
    .c_ack2(p2_ack),
    .c_data1(data1),
    .c_data2(data2)
  );

  // Periférico 1
  reg p1_clk;
  reg p1_rst;
  
  PER1 per1(
    .p_rst(p1_rst),
    .p_clk(p1_clk),
    .p_send(c_send),
    .p_ack(p1_ack),
    .p_data(data1)
  );
  
  // Periférico 2
  reg p2_clk;
  reg p2_rst;
  
  PER2 per2(
    .p_rst(p2_rst),
    .p_clk(p2_clk),
    .p_send(c_send),
    .p_ack(p2_ack),
    .p_data(data2)
  );

  // Clocks
  always #10  c_clk <= ~c_clk;
  always #17 p1_clk <= ~p1_clk;
  always #8  p2_clk <= ~p2_clk;

  initial
	begin
      $dumpfile("dump.vcd");
      $dumpvars(0, testbench);

      // Init

      c_clk = 0;
      c_rst = 1;

      p1_clk = 0;
      p1_rst = 1;
      
      p2_clk = 0;
      p2_rst = 1;

      #30

      // Start
      
      c_rst = 0;
      p1_rst = 0;
      p2_rst = 0;

      #300 $finish; // End simulation
    end
endmodule