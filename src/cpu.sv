`timescale 1ns / 1ps

module CPU (c_send, c_ack1, c_ack2, c_data1, c_data2, c_clk, c_rst);
  input c_ack1; // c_ack do periférico 1
  input c_ack2; // c_ack do periférico 2
  input c_clk; // clock
  input c_rst; // reset
  
  output reg [1:0] c_send; // c_send de envio
  
  output reg [15:0] c_data1; // dados enviados per 1
  output reg [15:0] c_data2; // dados enviados per 2
  
  reg c_stt1; // estado atual per 1
  reg c_nstt1; // próximo estado per 1
  
  reg c_stt2; // estado atual per 2
  reg c_nstt2; // próximo estado per 2
  
  // Next State
  always @ (*)
  begin
    case (c_stt1)
      // s=0
      0:
        begin
          if (c_ack1 == 0) c_nstt1 = 1;
        end
      // s=1
      1:
        begin
          if (c_ack1 == 1) c_nstt1 = 0;
        end
    endcase
    
    case (c_stt2)
      // s=0
      0:
        begin
          if (c_ack2 == 0) c_nstt2 = 1;
        end
      // s=1
      1:
        begin
          if (c_ack2 == 1) c_nstt2 = 0;
        end
    endcase
  end
  
  // State
  always @ (posedge c_clk)
  begin
    if (c_rst == 1)
      begin
        c_stt1 <= 0;
        c_stt2 <= 0;
        c_data1 <= 0;
        c_data2 <= 0;
      end
    else
      begin
        c_stt1 <= c_nstt1;
        c_stt2 <= c_nstt2;
      end
  end

  // Outputs
  always @ (*)
  begin
    c_send = {c_stt1, c_stt2};
  end
  
  // Update data1
  always @ (c_stt1)
  begin
    if (c_stt1 == 1)
      c_data1 <= c_data1 + 1;
  end
  
  // Update data2
  always @ (c_stt2)
  begin
    if (c_stt2 == 1)
      c_data2 <= c_data2 + 1;
  end
endmodule