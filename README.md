# Trabalho Grau A - Arquitetura e Organização de Computadores II

Trabalho por Lucas Müller e Luis Eich.

## FSM - Máquina de estados

### Periférico 1

![FSM Periférico 1](images/fsm-periferico1.jpg)

### Periférico 2

![FSM Periférico 2](images/fsm-periferico2.jpg)

### CPU

![FSM CPU](images/fsm-cpu.jpg)

O output da CPU será o _send_, que tem como valor a concatenação do _S1_ com _S2_.

## Desenvolvimento

O trabalho foi integralmente desenvolvido no EDA Playground utilizando a linguagem Verilog.

Os códigos estão disponível neste projeto, tanto de _design_ quanto de _testbench_.

Também está disponível em: https://www.edaplayground.com/x/4Qps

## Resultado

Após ser executado o trabalho no EDA Playground, obtivemos o seguinte resultado no EPWave:

![Resultado no EPWave](images/result.png)

O arquivo de _dump_ está disponível no projeto.

Para a execução do script, primeiramente é feito o reset e inicialização dos valores. Aos 30ps é que o código é executado, começando com o _send_ da CPU para os dois periféricos, visível aos 30ps, quando acontece o primeiro _clock_ após a inicialização.

PS: a cada _send_, o valor binário do _data_ correspondente é incrementado em 1, para que seja visível a mudança do dado enviado pela CPU no periférico.